from django.contrib.sites.models import Site


class SubdomainMiddleware(object):
    def __init__(self, get_response=None):
        self.get_response = get_response
        # One-time configuration and initialization.
        self.domain = Site.objects.get_current().domain

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        host = request.get_host()
        if host.endswith(self.domain):
            sub_domain = host[:-(len(self.domain) + 1)]
            if sub_domain == 'www' or sub_domain == '':
                sub_domain = None
                print 'Treating blank or "www" as not a sub-domain'
        else:
            sub_domain = None
            print 'Request origin did not match the domain we are serving'

        request.sub_domain = sub_domain
        request.domain = self.domain

        # Process the request
        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response
